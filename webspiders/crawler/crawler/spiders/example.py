from tracemalloc import start
import scrapy


class ExampleSpider(scrapy.Spider):
    name = "example"
    allowed_domains = ["csdn"]
    start_urls = ["http://test.trx.helipay.com/trx/merchantEntry/interface.action"]

    def start_requests(self):
        """post请求"""
        return [scrapy.FormRequest(url=self.start_urls[0], formdata={"interfaceName": "infoAlteration"}, callable=self.parse)]

    def parse(self, response):
        print(response.status_code)
