import unittest

from RequestClient.Src.Client.HttpGet import HttpGet

url_input = str(input("insert\n"))
http_request = HttpGet("")
http_request.set_url(url_input)


class RequestTest(unittest.TestCase):
    def test_http_request(self):
        self.assertEqual(http_request.http_get(), 200)


if __name__ == '__main__':
    unittest.main()
