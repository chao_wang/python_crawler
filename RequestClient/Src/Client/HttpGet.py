import httpx
from loguru import logger


class HttpGet:
    def __init__(self, *url: str):
        if len(url) == 0:
            self.__url = ""
        elif len(url) == 1:
            self.__url = url

    def set_url(self, url: str):
        if isinstance(url, str):
            self.__url = url
        else:
            self.__url = str(url)

    def get_url(self):
        return self.__url

    headers = {
        "user-agent": "Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; BLA-AL00 Build/HUAWEIBLA-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN"
        # 设置请求头
    }

    def config_error_log(self, error_content):
        logger.add("error_{time}.log")
        logger.error("错误内容：", error_content)

    def http_get(self):
        try:
            r = httpx.get(url=self.__url, headers=self.headers, timeout=5)
            logger.info("请求网址：%s;应答码:%s" % (self.__url, str(r.status_code)))
            return r.status_code
        except Exception as E:
            self.config_error_log(E)
