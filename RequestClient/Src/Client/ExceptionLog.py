import httpx
from loguru import logger


class ExceptionLog:
    """
    输出异常日志
    """
    logger.add("logfile_{time}.log")

    def __int__(self, content):
        self.__content = content

    def set_content(self, content):
        self.__content = content

    def output_log(self):
        logger.info(self.__content)


if __name__ == '__main__':
    try:
        TestLog = ExceptionLog()
        TestLog.set_content("测试方法")
        TestLog.output_log()
    except Exception as e:
        logger.error("错误：", e)
