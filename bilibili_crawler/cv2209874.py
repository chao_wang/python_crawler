import time
import requests
from lxml import etree


url = "https://www.bilibili.com/read/cv2209874"
payload = {"from": "articleDetail"}
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0"
}
r = requests.get(url, headers=headers, params=payload)
if r.status_code == 200:
    html = etree.HTML(r.text)
else:
    print("找不到网页")


imgs = html.xpath('//figure[@class="img-box"]/img/@data-src')
a = 1
for img in imgs:
    img_url = "http:" + img
    new_r = requests.get(img_url, headers=headers)
    if new_r.status_code == 200:
        path = r"D:\chao_wang\pythonfile\unitted\bilibili_img" + "\\" + str(a) + ".jpg"
        with open(path, "wb") as f:
            f.write(new_r.content)
            a += 1
            time.sleep(1)
    else:
        print("图片丢失啦！")


# 之前的代码太垃圾，废弃，重写了
