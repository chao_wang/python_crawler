import re
import time
import httpx

a = 1
url = "https://www.dgtle.com/article-1636974-1.html"
user_gent = {
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; rv:10.0) Gecko/20100101 Firefox/10.0"
}
res = httpx.get(url, headers=user_gent)
html = res.text
Img = re.findall(r"(http://s1.dgtle.com/dgtle_img/article/2020/12/26.*?jpeg)", html)
for images in Img:
    imgs = httpx.get(images)
    file_name = r"pics/{}.jpeg".format(a)
    with open(file_name, "wb") as j:
        j.write(imgs.content)
        a += 1
        time.sleep(1.2)
