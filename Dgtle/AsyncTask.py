import asyncio
import requests

async def http_request() -> str:
    r = requests.get("http://www.10086.cn/support/selfservice/sfzjm/")
    print(r.status_code)


async def main():
    print("程序开始")
    await http_request()
    print("结束")

asyncio.run(main())