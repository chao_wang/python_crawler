import time
import os

import requests
from lxml import etree

if not os.path.exists("Pictures"):
    path = os.mkdir("Pictures")


def Crawler():
    # url = input("爬取网址\n")
    url = "https://www.dgtle.com/article-1652986-1.html"
    req = requests.get(
        url=url,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0"
        },
    )
    if req.status_code == 200:
        return req.text


def Content_Parser():
    tree = etree.HTML(Crawler())
    img = tree.xpath("/html/body/div[2]/div[3]/div/div[2]/figure/img/@data-original")
    for imgs in img:
        # print(type(str(imgs)))
        img_req = requests.get(
            url=imgs,
            headers={
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0"
            },
        )
        # print(img_req)
        picture_name = str(imgs).split("/")
        # print(picture_name[-1])
        with open(
            f"D:\\Users\\wangc\\PycharmProjects\\python_crawler\\Dgtle\\Pictures\\{picture_name[-1]}",
            "wb",
        ) as files:
            files.write(img_req.content)
            time.sleep(0.8)


if __name__ == "__main__":
    Content_Parser()
